CoDeSys+L   �         �         @                �G�b                         
 �    ,   0   +      K   ����( �       K   �      K   �      K   �      K   �                  �          +     ��localhost 6 ��6 �b6     s       ��     �                              8%w�4%wq:E ������   ��@   ���)    ���%   q:E �� �f�  ?�� ���     c � pOu�          q:E     4           q:E �� �f�  ?�� �� 	   pOu�� ���                  pQuU�     ,   ,                                                        K          �G�b�        ��������        /*ABB2CONF 4*/
              ����hB   �G	�G�b     ��������           VAR_GLOBAL
END_VAR
                                                                                  "     ��������              y��S                 $����  ��������               ��������           Standard ���S	���S      ��������                         	iu�T     ��������           VAR_CONFIG
END_VAR
                                                                                   '              ,   L           Global_Variables y��S	 �	T     ��������          VAR_GLOBAL
   (* Entradas PLC *)
   IW1_Posicion : WORD;   (* Posici�n real de la valvula *)
   IX2_Error : BOOL;         (* Error en la V�lvula *)
    IW2_Caudal : WORD;  (*Lectura del caudal�metro *)
   IX3_Boton : BOOL;    (* puesta en marcha del proceso secuancial *)

 (* Variables de entrada  asociadas a la visualizaci�n *)
  (* Panel de Control de la V�lvula *)
   Man : BOOL;  (* Manual/Automarico *)
   RefManual : REAL;  (* SP Manual en lazo abierto*)
   (* Panel de Control del PI *)
   ManPI : BOOL;  (* Manual/Automatico PI *)
   ManPI_Ent: REAL;  (* Valor de entrada del PI Manual *)
   KpPI : REAL := 0.2; (*Parametro Kp del PI *)
   TNPI:REAL := 400;  (* Tiempo Integral del PI *)
  SPPI:REAL := 30;  (* Set Point del PI en modo lazo cerrado *)


   (*Salidas *)
   QW1_SPPosicion : WORD;  (* Consigna de posici�n para la v�lvula *)

 (*-----------------------------------------------------------------------------------*)

(* Otras variables para la simulaci�n *)
   QX3_ValvulaONOFF: BOOL; (* Orden Abre/Cierra Valvula ON/OFF *)

    (* Variables reales para entradas y salidas *)
    Posicion : REAL;   (*IW1_Posicion  en REAL *)
    Caudal : REAL;   (*IW2_Posicion  en REAL *)
    SPPosicion: REAL; (* QW1_SPPosicion en REAL *)

END_VAR
                                                                                               '           	     ��������           Variable_Configuration y��S	y��S	     ��������           VAR_CONFIG
END_VAR
                                                                                                    |0|0         ~      �   ���  �3 ���   � ���                  DEFAULT             System         |0|0   HH':'mm':'ss   dd'-'MM'-'yyyy'            E     ��������           ControladorPI y��S	y��S      ��������          FUNCTION_BLOCK ControladorPI
VAR_INPUT
	Y :REAL;       (* Variable a controlar. *)
	SP : REAL;    (* Set Point, valor deseado de la variable a controlar.*)
	Kp : REAL := 1;     (* Par�metro del controlador. Proporcionar un valor por defecto de 1.0.*)
       TN : REAL:=1;       (* Par�metro del controlador *)
	Umin  : REAL :=0;   (* Valor m�nimo de la salida. Por defecto tendr� el valor 0 *)
	UMax : REAL:=100;   (* Valor m�ximo de la salida. Por defecto, tendr� un valor 100.0.*)
	Offset :REAL:=0;     (* Cantidad a sumar a la se�al del controlador. Por defecto tendr� un valor 0.*)
	Manual : BOOL;      (* TRUE modo manual, FALSE modo autom�tico*)
	Manual_Ent : REAL;   (*Valor para la salida seg�n ecuaci�n (1.3) en modo manual *)
END_VAR
VAR_OUTPUT
	U : REAL;    (*Salida del controlador. *)
      Lim : BOOL;  (*Estar� a TRUE si se superan los l�mites Umin o Umax en la salida *)
END_VAR
VAR
	tact: TIME;
	tactw: DWORD;
	tinc: REAL;
	toldw: DWORD;
	error: REAL;
	ierror: REAL;
      Ciclo1:BOOL :=TRUE;
END_VAR`     (* Calcula el instante de tiempo actual y se pasa a DWORD en milisegundos *)
    tact:=TIME();
    tactw:=TIME_TO_DWORD(tact);
    (* Calcula el intervalo de tiempo desde el �ltimo ciclo en milisegundos *)
    tinc:=DWORD_TO_REAL(tactw-toldw);
    (* Almacena el valor actual para al pr�ximo ciclo *)
    toldw:=tactw;

IF Manual THEN
   U:=Manual_Ent+Offset;
   ierror:=0;
ELSE

    (* Calculo error �*)
   error:=SP-Y;
   (* Calculo de la integral del error  usando el m�todo de Euler*)
   IF NOT Ciclo1 THEN
       ierror:=ierror+tinc*error;
  END_IF

   (* Calculo de la salida del conttrolador *)
  U:=Kp*(error+(1/TN)*ierror)+Offset;


END_IF


IF Ciclo1 THEN
   U:=Offset;
   Ciclo1:=FALSE;
END_IF


IF U>Umax THEN
     U:=Umax;
ELSE
   IF U<Umin THEN
       U:=Umin;
   END_IF
END_IF

LIM:=(U>Umax) OR  (U<Umin);               ,   , � r �           ControlValvula y��S	y��S      ��������        V  FUNCTION_BLOCK ControlValvula
VAR_INPUT
    Manual : BOOL;
    SPManual : REAL;
    SPAuto : REAL;
    PV : REAL;
    ErrorValvula : BOOL;
    SPError : REAL;
     Encl : BOOL;
     SPEncl : REAL;
END_VAR
VAR_OUTPUT
      SP:REAL;
      DIF : REAL;
      Alarma : BOOL;
END_VAR
VAR
	TempoError: TON;
	INTempo: BOOL;
END_VARd  

IF MANUAL THEN
     SP:=SPManual;
ELSE
     SP:= SPAuto;
END_IF

(* Se le da prioridad al enclavamiento *)
IF Encl THEN SP:=SPEncl;  END_IF

DIF:=SP-PV;
INTempo:= ABS(DIF)>0.3;
TempoError(IN:=INTempo, PT:=T#10h);

Alarma:=ErrorValvula OR TempoError.Q;
(* Prioridad m�xima a la salida de error *)
IF (Alarma) THEN  SP:=SPError;   END_IF
               .   , ��f ��           Escalado y��S	y��S      ��������        H  FUNCTION_BLOCK Escalado
VAR_INPUT
	Valor_Ent : REAL;    (* Valor a convertir *)
	Min_Ent : REAL;      (* Valor m�nimo de la entrada *)
	Max_Ent : REAL;	  (* Valor m�ximo de la entrada *)
	Min_Sal : REAL;      (* Valor m�nimo de la salida *)
	Max_Sal : REAL;	   (* . Valor m�ximo de la salida. *)
       ValError : REAL:=-1;    (*Valor al que poner la salida en caso de error *)
END_VAR
VAR_OUTPUT
	Valor_Sal : REAL;   (*. Valor convertido *)
	Error :  BOOL;         (* Se activa cuando el valor de entrada queda fuera de l�mites *)
END_VAR
VAR
	pendiente: REAL;
END_VAR�   Error:= (Valor_Ent>Max_Ent) OR (Valor_Ent<Min_Ent);

IF NOT Error THEN
   pendiente:=(Max_Sal-Min_Sal)/(Max_Ent-Min_Ent);
   Valor_Sal:=Valor_Ent*pendiente+Min_Sal;
ELSE
   Valor_Sal:=ValError;
END_IF               /   , s : ��           EscaladoSenales y��S	y��S      ��������        R   PROGRAM EscaladoSenales
VAR
	EscalaPos: Escalado;
	EscalaSP: Escalado;
END_VAR      	EscalaPosAIW1_PosicionWORD_TO_REAL040950100A Escalado         Posicion     	EscalaPosA
IW2_CaudalWORD_TO_REAL040950100A Escalado         Caudal     EEscalaSP
SPPosicion010004095A Escalado       REAL_TO_WORD  QW1_SPPosiciond                  +   , 0 4 m�           PLC_PRG ���S	y��S      ��������           PROGRAM PLC_PRG
VAR
END_VAR:    Proceso();
  EscaladoSenales();
 ProgramaControl();

               F   , � " ^A           Proceso y��S	y��S      ��������        P  PROGRAM Proceso
VAR
       (* Par�metros del sistema posici�n v�lvula *)
       K: REAL :=1;
       tau : REAL:=500;
       (* Par�metros del sistema Posici�n-Caudal *)
       K1: REAL :=0.6;
       tau1 : REAL:=3000;
      (* Variables de tiempo *)
	tact: TIME;
	tactw: DWORD;
	tinc: REAL;
	toldw: DWORD;
      (* Variables del Proceso *)
      Pos : REAL;
     SP_Pos:REAL;
     Caudal:REAL;
      Ciclo1:BOOL:=TRUE;
	EscalaSalida: Escalado;
	IW1R: REAL;
      IW2R:REAL;
	EscalaEntrada: Escalado;
	QW1R: REAL;
	EscalaPos: Escalado;
	EscalaCaudal: Escalado;
END_VAR�    (* Lectura de la consigna de la posici�n y paso a real *)
  QW1R:=WORD_TO_REAL(QW1_SPPosicion);
  EscalaEntrada(Valor_Ent:=QW1R,Min_Ent:=0,Max_Ent:=4095,Min_Sal:=0,Max_Sal:=100,Valor_Sal=>SP_Pos);

    (* Programa que simula un sistema de primer orden *)
    tact:=TIME();
    tactw:=TIME_TO_DWORD(tact);
    (* Calcula el intervalo de tiempo desde el �ltimo ciclo en milisegundos *)
    tinc:=DWORD_TO_REAL(tactw-toldw);
    (* Almacena el valor actual para al pr�ximo ciclo *)
    toldw:=tactw;



    IF Ciclo1=FALSE THEN
        Pos:=Pos+tinc*(K*SP_Pos-Pos)/tau;
        Caudal:=Caudal+tinc*(K1*Pos-Caudal)/tau1;
    ELSE
        Ciclo1:=FALSE;
    END_IF

    EscalaPos(Valor_Ent:=Pos,Min_Ent:=0,Max_Ent:=100,Min_Sal:=0,Max_Sal:=4095,Valor_Sal=>IW1R);
    IW1_Posicion:=REAL_TO_WORD(IW1R);
     EscalaCaudal(Valor_Ent:=Caudal,Min_Ent:=0,Max_Ent:=100,Min_Sal:=0,Max_Sal:=4095,Valor_Sal=>IW2R);
    IW2_Caudal:=REAL_TO_WORD(IW2R);               0   , G V 
           ProgramaControl ���S	���S      ��������        �   PROGRAM ProgramaControl
VAR
	Valvula1: ControlValvula;
      PICaudal: ControladorPI;
	Diferencia: REAL;
	LuzValvula: BOOL;
END_VAR      Valvula1Man	RefManualPICaudalCaudalSPPIKpPITNPI01000ManPIA	ManPI_EntControladorPI       Posicion	IX2_Error100QX3_ValvulaONOFFA0ControlValvula  
Diferencia 
LuzValvula      
SPPosiciond                   K   , � x �           Visu y��S    .   d   ,                                                                                                          �� 's�6  �э     ���                                                                     ���                                                                                                                                          ��                              ���           Man                       �                                                                                                  0   100    �J"e�W  ���     ���                                     	   RefManual   %4.1f                        ���                                 M  < � �h� "       2   '  �  �  '  �        HH':'mm':'ss   dd'-'MM'-'yyyy                  �             1    Arial          ���     �   ��   �   ��   � � � ���     �   ��   �   ��   � � � ���        0   100   20   20   10   20                  �             1    Arial          ���     �   ��   �   ��   � � � ���     �   ��   �   ��   � � � ���          �    Proceso.Caudal             Proceso.Pos        �   ��   �   ��   � � � ���     �   ��   �   ��   � � � ���            �      7.5  �    2.5    �   ��   �   ��   � � � ���     �   ��   �   ��   � � � ���                                                                                                           T� �� ��     �     ���                                                                     ���                                                                                                                                             �� �� �� �� ��    �      �                                                 	   IX2_Error    ���                                                                                                                                            � �� � �    �      �                                                 	   IX2_Error    ���                                                                                                                                          � �� ��     �     ���                                                                     ���                                                                                                                                            �V  o �b    �      �                                   	   IX2_Error       M                        ���                                                                                                                                            �o �� ��    �      �                                   	   IX2_Error                            	    ���                                                                                                                                   0   100    �� &� ��   ���     ���                                     
   SPPosicion   %4.1f                    
    ���                                                                                                                                            #� D� 3�     ���     ���                                            SP                        ���                                                                                                                                            &� G� 6�     ���     ���                                            Pos                        ���                                                                                                                                            �"A�1    ���     ���                                            Manual                        ���       Arial                                                                                                                           0   100    �� (� ��   ���     ���                                        Proceso.Pos   %4.1f                        ���                                                                                                                                          < Q F                              ���        	   IX2_Error                       �                                                                                                           < =T ,H     ���     ���                                            Error                        ���                                                                                                                                            � E6 %     ���     ���                                         	   Valvula 1                        ���       Arial                                                                                                                                     M� @� T� T�    �      �                                                    QX3_ValvulaONOFF    ���                                                                                                                                           6� ,� ,� A� A�    �      �                                                    QX3_ValvulaONOFF    ���                                                                                                                                          @n E� B�    �      �                                      QX3_ValvulaONOFF                                ���                                                                                                                                            ,n Uu @q    �      �                                      QX3_ValvulaONOFF                                ���                                                                                                                                            � � -� � �     �     ���                                                                     ���                                                                                                                                            2 }e @K     ���     ���                                            Valvula 
ON/OFF                        ���       Arial                                                                                                                                  6� K� @�                              ���           QX3_ValvulaONOFF                       �                                                                                                           "� i� E�     ���     ���                                            Abrir/cerrar                        ���                                                                                                                                            � r-�� �    ���     ���                                            Caudal                        ���       Arial                                                                                                                                   �F �o �Z   ���     ���                                            FT                        ���                                                                                                                                            �n �� �}   ���     ���                                                                     ���                                                                                                                                           l� I��  ���     ���                                                                     ���                                                                                                                                          �� �� ��                              ���           ManPI                        �                                                                                                           ��4���    ���     ���                                            Controlador PI                         ���       Arial                                                                                                                                    �� !� ��     ���     ���                                            Manual/Auto                    "    ���       Arial                                                                                                                                    �� ?�     ���     ���                                         	   SP Manual                    #    ���       Arial                                                                                                                           0   100    �� ���   ���     ���                                     	   ManPI_Ent   %4.1f                    $    ���                                                                                                                                  0   100    ��3�%  ���     ���                                        KpPI   %4.1f                    %    ���                                                                                                                                  0   1000    �6�Q�C  ���     ���                                        TNPI   %4.1f                    &    ���                                                                                                                                           �!7�'    ���     ���                                            Kp                    '    ���       Arial                                                                                                                                    �6!U�E    ���     ���                                            TN                    (    ���       Arial                                                                                                                           0   100    �P Nk #]   ���     ���                                        Proceso.Caudal   %4.1f                    )    ���                                                                                                                                   0   100    �`�{�m  ���     ���                                        SPPI   %4.1f                    *    ���                                                                                                                                            �]"|�l    ���     ���                                            SP                    +    ���       Arial                                                                                                                                    ` ^ > >                     Boton    ,    ���            	   IX3_Boton                   �                                                                                                           	 S r � = q     ���     ���                                            Arranque Proceso
Secuencial                    -    ���                                          �   ��   �   ��   � � � ���     �   ��   �   ��   � � � ���                  ����, c$ #�            Util.lib*6.7.22 21:38:48 @8G�b    IecSfc.lib*6.7.22 21:38:59 @CG�b,   Save_V40\Standard.lib*18.5.98 13:58:24 @Є`5   
   BCD_TO_INT      BLINK   	   CHARCURVE   
   DERIVATIVE      EXTRACT      FREQ_MEASURE      GEN   
   HYSTERESIS   
   INT_TO_BCD      INTEGRAL   
   LIMITALARM   	   LIN_TRAFO      PACK      PD      PID      PID_FIXCYCLE      PUTBIT      RAMP_INT   	   RAMP_REAL      STATISTICS_INT      STATISTICS_REAL      UNPACK      VARIANCE      Version_Util         Globale_Variablen          SFCActionControl          Globale_Variablen          CONCAT       CTD       CTU       CTUD       DELETE       F_TRIG       FIND       INSERT       LEFT       LEN       MID       R_TRIG       REPLACE       RIGHT       RS       SEMA       SR       TOF       TON       TP          Global Variables 0                      ��������           2 �  �           ����������������  
             ����  ��������        ����  ��������                      POUs                 ControladorPI  E                   ControlValvula  ,                   Escalado  .                   EscaladoSenales  /                   PLC_PRG  +                   Proceso  F                  ProgramaControl  0   ����          
   Data types  ����              Visualizations                Visu  K   ����              Global Variables                 Global_Variables                     Variable_Configuration  	   ����                                         ��������             ���S               *                 	   localhost            P      	   localhost            P      	   localhost            P     ���S   ��d